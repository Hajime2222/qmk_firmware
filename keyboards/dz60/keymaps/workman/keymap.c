#include "workman.h"

#define ____ KC_TRNS

#define _BL 0
#define _FL1 1
#define _NUM 2
#define _SHORTCUTS 3
#define _QWERTY 10
#define _LED_SOUND 11


#define _COPY LCTL(KC_C)
#define _PASTE LCTL(KC_V)
#define _CUT LCTL(KC_X)

#define _UNDO LCTL(KC_Z)
#define _REDO LCTL(LSFT(KC_Z))
#define _SAVE LCTL(KC_S)



enum {
  TD_COL_SEMC = 0,
	TD_CTRL_CTRLSFT,
	TD_IME_ENT,
  TD_QUOTATION
};

qk_tap_dance_action_t tap_dance_actions[] = {
  [TD_COL_SEMC]  = ACTION_TAP_DANCE_DOUBLE(KC_COLN, KC_SCLN),
	// [TD_CTRL_CTRLSFT]  = ACTION_TAP_DANCE_DOUBLE(OSM(MOD_LCTL), OSM(MOD_LSFT)),
	[TD_IME_ENT]  = ACTION_TAP_DANCE_DOUBLE(LCTL(LSFT(KC_F9)), KC_ENT),
	[TD_QUOTATION]  = ACTION_TAP_DANCE_DOUBLE(KC_QUOT,LSFT(KC_QUOT))
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

/* Layer 0
* ,-----------------------------------------------------------------------------------------.
* | Esc |  1  |  2  |  3  |  4  |  5  |  6  |  7  |  8  |  9  |  0  |  -  |  =  |    Bck    |
* |-----------------------------------------------------------------------------------------+
* | Tab   |  Q  |  D  |  R  |  W  |  B  |  J  |  F  |  U  |  P  |  ;  |  [  |  ]  |    \    |
* |-----------------------------------------------------------------------------------------+
* | Caps    |  A  |  S  |  H  |  T  |  G  |  Y  |  N  |  E  |  O  |  I  |  '
* |-----------------------------------------------------------------------------------------+
* | Shift     |  Z  |  V  |  M  |  C  |  X  |  K  |  L  |  ,  |  .  |    RSh    |  ↑  |  /  |
* |-----------------------------------------------------------------------------------------+
* | Ctrl  |  GUI |  Alt |     Space     |  Fn1 |    Space    | Pscr | Fn1 |  ←   |  ↓  |  →  |
* `-----------------------------------------------------------------------------------------'
*/



	[_BL] = MYLAYOUT(
		KC_ESC,  KC_1,    KC_2,   KC_3,   KC_4,  KC_5,  KC_6,  KC_7,  KC_8,  KC_9,    KC_0,    KC_MINS,  KC_EQL,   				   KC_BSPC,
		KC_TAB,           KC_Q,   KC_D,   KC_R,  KC_W,  KC_B,  KC_J,  KC_F,  KC_U,    KC_P,    KC_MINS,  KC_LBRC,  KC_RBRC,  KC_BSLS,
		OSM(MOD_LCTL),    KC_A,   KC_S,   KC_H,  KC_T,  KC_G,  KC_Y,  KC_N,  KC_E,    KC_O,    KC_I,  	 KC_QUOT,  				   TD(TD_IME_ENT),
		OSM(MOD_LCTL | MOD_LSFT), OSM(MOD_LSFT), KC_Z,   KC_V,   KC_M,  KC_C,  KC_X,  KC_K,  KC_L,  KC_COMM, KC_DOT,  KC_SLSH,	 KC_RSFT,  KC_UP,    MO(_LED_SOUND),

		KC_LCTL,   KC_LGUI,   KC_LALT,   			 LT(_FL1, KC_SPC),  LT(_SHORTCUTS, KC_ENT)  , 	LT(_NUM, KC_BSPC),					 KC_DEL, 		KC_APP,  			KC_LEFT, 	 KC_DOWN,	 	KC_RGHT),

	[_FL1] = MYLAYOUT(
		____,   ____,  	 			 ____, 	  ____, 	____,  ____,  								____,  ____, 				 ____, 	  ____,    ____,     ____,   					____,   						____,
		____,           			 ____,  	KC_LCBR, KC_RCBR,  ____,  						____,  ____, 				 ____, 	 KC_UP,    ____,     ____,  					____,  		____, 		____,
		____,          				 ____,    KC_LPRN, KC_RPRN,  TD(TD_COL_SEMC), 	____,  ____,			KC_LEFT, KC_DOWN, KC_RGHT,     KC_TAB,  					____,  							____,
		____, 	____, 				 ____,    KC_LBRC, KC_RBRC,  TD(TD_QUOTATION),  						____,  ____,  				____,   ____, 	 ____, 	   ____,						____,  		____,    	____,

		____,   ____,    ____,   						 ____,   ____, 	KC_TAB,						 ____, 		____,			  ____, 	 ____,	 	____),

	[_NUM] = MYLAYOUT(
		____,   ____,  	 			 ____,    ____, 	____,  ____,  	____,  ____, 				   ____, 	  ____,   ____,    ____,   					____,   						____,
		____,           			 KC_F1,  	KC_F2, 	KC_F3,  KC_F4,  	KC_F5,  KC_F6, 			 KC_F7, 	KC_F8,   KC_F9,    KC_F10,  			____,  		____, 		____,
		____,          				 KC_1,    KC_2,   KC_3,   KC_4, 	  KC_5,  KC_6,				 KC_7, 		KC_8,		 KC_9,     KC_0,  				____,  							____,
		____, 	____, 				 ____, ____,   ____,  ____, 		  ____,  ____,  				 KC_EQL,  KC_PPLS, KC_PMNS, KC_PAST,  			KC_PSLS,	____,    	____,

		____,   ____,    ____,   						 OSM(MOD_LSFT),   KC_BSPC, 	____,						 ____, 		____,			  ____, 	 ____,	 	____),


	[_SHORTCUTS] = MYLAYOUT(
		____,   ____,  	 			 ____,    		____, 	____,  ____,  				____,  ____, 			 ____, 	____,   ____,   ____,   		____,   						____,
		____,           			 ____,  			_CUT, 	_COPY,  _PASTE,  				____,  ____, 			 ____, 	____,   ____,   ____,  			____,  		____, 		____,
		____,          				 _SAVE,   		  ____,   _UNDO, _REDO,   ____,  ____,			 ____, 	____,		____,   ____,  			____,  							____,
		____, 	____, 				 ____, 				____,   ____,  ____, 					____,  ____,  		 ____,  ____, 	____, 	____,  			____,			____,     ____,

		____,   ____,    ____,   						 ____,   ____, 	____,						 ____, 		____,			  ____, 	 ____,	 	____),

	// [_SHORTCUTS] = MYLAYOUT(
	// 	____,   ____,  	 			 ____,    ____, 	____,  ____,  		____,  ____, 			 ____, 	____,   ____,   ____,   		____,   						____,
	// 	____,           			 ____,  	____, 	____,  ____,  		____,  ____, 			 ____, 	____,   ____,   ____,  			____,  		____, 		____,
	// 	____,          				 ____,    ____,   ____,  ____, 	    ____,  ____,			 ____, 	____,		____,   ____,  			____,  							____,
	// 	____, 	____, 				 ____, 		____,   ____,  ____, 			____,  ____,  		 ____,  ____, 	____, 	____,  			____,			____,     ____,
	//
	// 	____,   ____,    ____,   						 ____,   ____, 	____,						 ____, 		____,			  ____, 	 ____,	 	____),

	[_LED_SOUND] = MYLAYOUT(
		RESET,   ____,  	 			____, 	  ____, 	____,  ____,  		____,  ____, 				 ____, 	  ____,    ____,     ____,   					____,   						____,
		RGB_TOG,     					RGB_VAI, 	RGB_VAD, RGB_MOD, ____,  		____,  ____, 				 ____, 	  ____,    ____,     ____,  					____,  		____, 		____,
		KC_MUTE,          		KC_VOLU,  KC_VOLD,   ____,  ____, 		____,  ____,				 ____, 		____,		 ____,     ____,  					____,  							____,
		____, 	____, 				 ____,    ____,   ____,  ____, 		 	  ____,  ____,  			____,     ____, 	 ____, 	   ____,						____,  		TG(_QWERTY),    	____,

		____,   ____,    ____,   						 ____,   ____, 	____,						 ____, 		____,			  ____, 	 ____,	 	____),

	[_QWERTY] = MYLAYOUT(
		KC_ESC,   ____,  	 		____, 	____, 	____,  ____,  ____,  ____, 	____, ____,    ____,   ____,   					____,   						____,
		KC_TAB,     					KC_Q,   KC_W,   KC_E,  KC_R, 	KC_T,  KC_Y, 	KC_U, KC_I,    KC_O,   KC_P,    KC_LBRC,  KC_RBRC, KC_BSLS,
		KC_LCTL,          		KC_A,   KC_S,   KC_D,  KC_F,	KC_G,  KC_H,  KC_J, KC_K,	   KC_L,   KC_SCLN,	KC_QUOT,  KC_ENT,
		KC_LSFT, 	KC_LSFT, 				KC_Z,   KC_X,   KC_C,  KC_V, 	KC_B,  KC_N,  KC_M, KC_COMM, KC_DOT, KC_SLSH,	KC_RSFT,  ____,   	____,

		KC_LCTL,   KC_LGUI,    KC_LALT,   				KC_SPC,  LT(_SHORTCUTS, KC_ENT)  , 	LT(_NUM, KC_BSPC),					 ____, 		____,			  ____, 	 ____,	 	____),

};
